<?php

if ( ! defined( 'WP_CLI' ) ) {
    return;
}


class Sync extends WP_CLI_Command {


    public function __construct() {
    }


    /**
     * Uses WP Migrate DB Pro to sync a remote database and media files to a local site.
     *
     * Must set up wp-config.php, create database (`wp db create`), and install WordPress before running.
     *
     * ## OPTIONS
     * <remote_url>
     * : The remote site URL
     *
     * <secret_key>
     * : The remote site WP Migrate DB Pro Secret Key
     *
     * [--core]
     * : Only sync default WordPress core tables
     *
     * [--media]
     * : Also sync media files
     *
     * [--media-only]
     * : Only sync media files (doesn't sync database data)
     *
     * [--raw]
     * : Don't anonymize database data after sync
     *
     * [--update]
     * : Update the WP Migrate DB Pro plugins to the reference version; useful when you get a 'Please update WP Migrate DB Pro' error message
     *
     * [--woocommerce]
     * : Sync default WooCommerce core tables
     *
     * ## EXAMPLES
     *
     *     wp sync <remote_url> <secret_key>
     *     wp sync <remote_url> <secret_key> --media
     *     wp sync --core <remote_url> <secret_key>
     *
     */
    public function __invoke( $args, $assoc_args ) {
        if (2 != count($args)) {
             WP_CLI::error('Please provide WP Migrate DB Pro remote site URL and Key');
        }

        // TODO: run `wp db create`, `wp core install` if needed

        WP_CLI::log(WP_CLI::colorize('%cInstalling WordPress%n'));
        WP_CLI::runcommand('core install --url=example.com --title="Test" --admin_user=x --admin_email=x@example.com');

        $options = [
            'return' => 'return_code',
            'exit_error' => false,
        ];

        WP_CLI::log(WP_CLI::colorize('%cDownloading plugins%n'));
        if (isset($assoc_args['update']) || WP_CLI::runcommand('plugin is-installed wp-migrate-db-pro', $options)) {
            WP_CLI::runcommand('plugin install --force https://files.freshpreview.com/wp-migrate-db-pro.zip');
        }
        if (isset($assoc_args['update']) || WP_CLI::runcommand('plugin is-installed wp-migrate-db-pro-media-files', $options)) {
            WP_CLI::runcommand('plugin install --force https://files.freshpreview.com/wp-migrate-db-pro-media-files.zip');
        }
        if (isset($assoc_args['update']) || WP_CLI::runcommand('plugin is-installed wp-migrate-db-pro-cli', $options)) {
            $versionCheckOptions = [
                'return' => true,
                'exit_error' => false
            ];
            $migratedbpro_version = WP_CLI::runcommand('plugin list --field=version --name=wp-migrate-db-pro', $versionCheckOptions);

            // check the current version of wp-migrate-db-pro and download the correct cli version
            if (version_compare($migratedbpro_version, '1.7', '<')) {
                WP_CLI::error('Your version of WP Migrate DB Pro is too old; please update local and remote servers to a newer version');
            } else if (version_compare($migratedbpro_version, '1.8.1', '<')) {
                WP_CLI::runcommand('plugin install --force https://files.freshpreview.com/wp-migrate-db-pro-cli-1.3.zip');
            } else {
                WP_CLI::runcommand('plugin install --force https://files.freshpreview.com/wp-migrate-db-pro-cli-1.3.2.zip');
            }
        }
        WP_CLI::log(WP_CLI::colorize('%cActivating plugins%n'));
        WP_CLI::runcommand('plugin activate wp-migrate-db-pro');
        WP_CLI::runcommand('plugin activate wp-migrate-db-pro-cli');
        WP_CLI::runcommand('plugin activate wp-migrate-db-pro-media-files');

        WP_CLI::log(WP_CLI::colorize('%cChecking WP Migrate DB Pro license key%n'));
        if (!defined('WPMDB_LICENCE')) {
            WP_CLI::runcommand('migratedb setting update license `curl https://files.freshpreview.com/license.html`');
        }

        WP_CLI::log(WP_CLI::colorize('%cBeginning sync%n'));
        $tables_to_sync = array();
        $pull_command = 'migratedb pull';
        if (isset($assoc_args['media']) || isset($assoc_args['media-only'])) {
            $pull_command .= ' --media=compare';
        }
        if (isset($assoc_args['media-only'])) {
            global $wpdb;
            // links table is (usually) never used and contains no data
            $tables_to_sync[] = 'links';
        } else if (isset($assoc_args['core']) || isset($assoc_args['woocommerce'])) {
            if (isset($assoc_args['core'])) {
                $core_tables = array(
                    'commentmeta',
                    'comments',
                    'links',
                    'options',
                    'postmeta',
                    'posts',
                    'term_relationships',
                    'term_taxonomy',
                    'termmeta',
                    'terms',
                    'usermeta',
                    'users',
                );
                $tables_to_sync = array_merge($tables_to_sync, $core_tables);
            }
            if (isset($assoc_args['woocommerce'])) {
                $woocommerce_tables = array(
                    'woocommerce_api_keys',
                    'woocommerce_attribute_taxonomies',
                    'woocommerce_downloadable_product_permissions',
                    'woocommerce_log',
                    'woocommerce_order_itemmeta',
                    'woocommerce_order_items',
                    'woocommerce_payment_tokenmeta',
                    'woocommerce_payment_tokens',
                    'woocommerce_sessions',
                    'woocommerce_shipping_zone_locations',
                    'woocommerce_shipping_zone_methods',
                    'woocommerce_shipping_zones',
                    'woocommerce_tax_rate_locations',
                    'woocommerce_tax_rates',
                );
                $tables_to_sync = array_merge($tables_to_sync, $woocommerce_tables);
            }
        }
        if (!empty($tables_to_sync)) {
            array_walk($tables_to_sync, function (&$item, $key) { global $wpdb; $item = $wpdb->prefix . $item; });
            $pull_command .= ' --include-tables=' . implode(',', $tables_to_sync);
        }
        $pull_command .= ' ' . $args[0] . ' ' . $args[1];
        WP_CLI::runcommand($pull_command);

        if (!isset($assoc_args['media-only']) && !isset($assoc_args['raw'])) {
            WP_CLI::runcommand('sanitize db --yes');
        }

        WP_CLI::success('Sync complete.');
    }

}
WP_CLI::add_command('sync', 'Sync');

