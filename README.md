# WPCLI Sync #

Uses WP-CLI and WP Migrate DB Pro to sync database (and optionally media files) from a remote site.

Can be used to refresh data on an already running site or as part of setting up a new site for the first time.

WARNING: This will overwrite data in your local database.

### Installation ###

To install as a wp-cli package:

```
#!bash

wp package install https://bitbucket.org/freshconsulting/wpcli-sync/get/master.zip
```

Or to install manually clone this repo and add the path to the `sync.php` file to the `require` section of your `~/.wp-cli/config.yml` (see https://make.wordpress.org/cli/handbook/config/#config-files for details).


### Prerequisites ###

* Ensure that your `wp-config.php` file is created with the correct database name and credentials (`wp config create` if not)
* Ensure that the database referenced in the `wp-config.php` file exists (`wp db create` if not)
* Ensure that WordPress is installed (`wp core install` if not)
* Ensure WP Migrate DB Pro is set up on the remote site
* Copy "Connection Info" from remote site WP Migrate DB Pro "Settings" tab

![Migrate_DB_Pro.png](https://bitbucket.org/repo/BgBoK7e/images/413983288-Migrate_DB_Pro.png)


### To use ###


```
#!bash

wp sync <remote_url> <secret_key>
```
where `<remote_url>` and `<secret_key>` are copied from the remote site WP Migrate DB Pro "Settings" tab.

To include media files
```
#!bash

wp sync <remote_url> <secret_key> --media
```

To sync with a site protected by basic authentication add the username and password to the remote URL. E.g. `https://www.example.com` becomes `https://username:password@www.example.com`.


### Common Problems ###

If you get an error `Error: Activate remote license` then you may need to remove and re-add the WP Migrate DB Pro license key on the remote site. This is needed because WP Migrate DB Pro will automatically deactivate sites after 30 days of inactivity.
